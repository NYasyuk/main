import java.text.SimpleDateFormat;
import java.util.*;

public class Sale {
    SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm:ss");

    Date dateNow = new Date();

    Random random = new Random();

    Map<Integer, Client> queueClients = new LinkedHashMap();
    IceCreamMaker iceCreamMaker1 = new IceCreamMaker();
    IceCreamMaker iceCreamMaker2 = new IceCreamMaker();

    int clientNumber = 0;
    int satisfied = 0;
    int neutral = 0;
    int dissatisfied = 0;

    Date startSaling(int tFast, int tSlow, int tClient, int pClient, int tFull, int tWait) {
        int lastClientServed = -1;
        int tHalf = (tSlow - tFast) / 2;

        int diffForWait = tWait - tSlow + 1;
        int diffForServe = tSlow - tFast;

        Date dateEnd = new Date(dateNow.getTime() + (long) tFull);

        Date nextDateCome = new Date(dateNow.getTime() + (long) tClient);
        Date nextTimeLeave = dateNow;
        int nextClientNumberLeave = 0;

        do {
            //Client Come
            if (nextDateCome.before(dateNow)) {
                Client clientAdd = clientAddInQueue(pClient, tSlow, diffForWait);
                if (clientAdd != null) {
                    if (nextTimeLeave.after(clientAdd.leaveTime()) || queueClients.isEmpty()) {
                        nextTimeLeave = clientAdd.leaveTime();
                        nextClientNumberLeave = clientNumber - 1;
                    }
                }
                nextDateCome = new Date(dateNow.getTime() + (long) tClient);
            }

            sellerFreed(iceCreamMaker1, dateNow, tFast, tHalf, 1);
            sellerFreed(iceCreamMaker2, dateNow, tFast, tHalf, 2);

            lastClientServed = sellerStartServesClient(iceCreamMaker1, nextClientNumberLeave, lastClientServed, tFast, diffForServe, 1);
            lastClientServed = sellerStartServesClient(iceCreamMaker2, nextClientNumberLeave, lastClientServed, tFast, diffForServe, 2);

            //Client leave from queue
            if ((nextTimeLeave.before(dateNow) && !queueClients.isEmpty())
                    || lastClientServed == nextClientNumberLeave) {
                if (nextTimeLeave.before(dateNow) && queueClients.containsKey(nextClientNumberLeave))
                    clientLeaveFromQueue(dateNow, nextClientNumberLeave);
                if (!queueClients.isEmpty()) {
                    nextClientNumberLeave = takeNextLeaveClientFromQueue(dateNow);
                    nextTimeLeave = queueClients.get(nextClientNumberLeave).leaveTime();
                }
            }

            dateNow = new Date();
        } while (dateNow.before(dateEnd));

        System.out.println("\nДовольны: " + satisfied + "\nСпокойны: " + neutral + "\nНедовольны: " + dissatisfied);

        return new Date();
    }

    Client clientAddInQueue(int pClient, int tSlow, int diffForWait) {
        if (random.nextInt(100) < pClient) {
            Client clientAdd = new Client(dateNow, random.nextInt(diffForWait + 1) + tSlow);
            System.out.println("Пришёл клиент: " + clientNumber + "  Время прихода: " + dateFormat.format(clientAdd.getArrivalTime())
                    + "  Время ожидания: " + clientAdd.gettWait() / 1000 + " сек.  Время ухода: " + dateFormat.format(clientAdd.leaveTime()));

            clientNumber++;
            return queueClients.put(clientNumber - 1, clientAdd);
        }
        return null;
    }

    Client clientLeaveFromQueue(Date dateNow, int nextClientNumberLeave) {
        System.out.println("\u001B[31m\nКлиент " + nextClientNumberLeave + " ушёл  Время: " + dateFormat.format(dateNow) + " (Клиент остался недоволен)\n\u001B[0m");
        dissatisfied++;
        return queueClients.remove(nextClientNumberLeave);
    }

    int sellerStartServesClient(IceCreamMaker iceCreamMaker, int nextClientNumberLeave, int lastClientServed, int tFast, int diffForServe, int numberIceCreamMaker) {
        if (!queueClients.isEmpty()) {
            if (iceCreamMaker.getClient() == null) {
                lastClientServed = queueClients.keySet().iterator().next();
                iceCreamMaker.startServeCustomer(queueClients.get(lastClientServed), lastClientServed, random.nextInt(diffForServe + 1) + tFast);

                queueClients.remove(lastClientServed);
                System.out.println("\u001B[34m\nКлиент " + lastClientServed + " обслуживается продавцом " + numberIceCreamMaker + "  Время начала обслуживания: " + dateFormat.format(dateNow)
                        + "  Время приготовления: " + iceCreamMaker.getTimeForOrder() / 1000 + " сек.  Время выдачи заказа: " + dateFormat.format(iceCreamMaker.endTimeForOrder()) + "\n\u001B[0m");
            }
        }
        return lastClientServed;
    }

    //take the next client who will leave the queue before everyone else
    int takeNextLeaveClientFromQueue(Date dateNow) {
        int nextClientNumberLeave = 0;
        Date nextTimeLeave = dateNow;

        for (Map.Entry<Integer, Client> entry : queueClients.entrySet()) {
            if (nextTimeLeave.after(dateNow)) {
                if (nextTimeLeave.after(entry.getValue().leaveTime())) {
                    nextTimeLeave = entry.getValue().leaveTime();
                    nextClientNumberLeave = entry.getKey();
                }
            } else {
                nextTimeLeave = entry.getValue().leaveTime();
                nextClientNumberLeave = entry.getKey();
            }
        }

        return nextClientNumberLeave;
    }

    void sellerFreed(IceCreamMaker iceCreamMaker, Date dateNow, int tFast, int tHalf, int numberIceCreamMaker) {
        if (iceCreamMaker.getClient() != null && iceCreamMaker.endTimeForOrder().before(dateNow)) {
            if (tFast < iceCreamMaker.getTimeForOrder() && tFast + tHalf >= iceCreamMaker.getTimeForOrder()) {
                satisfied++;
                System.out.println("\u001B[32m\nКлиент " + iceCreamMaker.getClientNumber() + " был обслужен продавцом " + numberIceCreamMaker + "  Время начала обслуживания: " + dateFormat.format(dateNow)
                        + "  Время приготовления: " + iceCreamMaker.getTimeForOrder() / 1000 + " сек.  Время выдачи заказа: " + dateFormat.format(dateNow) + " (Клиент остался доволен)\n\u001B[0m");
            } else {
                neutral++;
                System.out.println("\u001B[33m\nКлиент " + iceCreamMaker.getClientNumber() + " был обслужен продавцом " + numberIceCreamMaker + "  Время начала обслуживания: " + dateFormat.format(dateNow)
                        + "  Время приготовления: " + iceCreamMaker.getTimeForOrder() / 1000 + " сек.  Время выдачи заказа: " + dateFormat.format(dateNow) + " (Клиент остался удволетворён)\n\u001B[0m");
            }
            iceCreamMaker.endServeCustomer();
        }
    }
}

