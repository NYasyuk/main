import java.util.Date;

public interface Seller {
    public Date startServeCustomer(Client client, int clientNumber, int timeForOrder);

    public void endServeCustomer();

    public Date endTimeForOrder();
}
