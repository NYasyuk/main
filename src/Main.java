import java.io.Console;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("\n");
        Sale sale = new Sale();
        sale.startSaling(Configuration.tFast, Configuration.tSlow, Configuration.tClient,
                Configuration.pClient, Configuration.tFull, Configuration.tWait);

    }
}
