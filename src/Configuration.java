public class Configuration {
    static int tFast = 1000;
    static int tSlow = 30000;
    static int tClient = 1000;
    static int pClient = 50;
    static int tFull = 120000;
    static int tWait = 40000;
}
