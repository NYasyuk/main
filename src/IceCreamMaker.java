import java.util.Date;

public class IceCreamMaker implements Seller {
    private Client client;
    private Date serveTime;
    private int timeForOrder;
    private int clientNumber;

    public int getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(int clientNumber) {
        this.clientNumber = clientNumber;
    }

    public Date getServeTime() {
        return serveTime;
    }

    public void setServeTime(Date serveTime) {
        this.serveTime = serveTime;
    }

    public int getTimeForOrder() {
        return timeForOrder;
    }

    public void setTimeForOrder(int timeForOrder) {
        this.timeForOrder = timeForOrder;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date startServeCustomer(Client client, int clientNumber, int timeForOrder) {
        this.client = client;
        this.clientNumber = clientNumber;
        serveTime = new Date();
        this.timeForOrder = timeForOrder;

        return new Date(client.getArrivalTime().getTime() + (long) timeForOrder);
    }

    public void endServeCustomer() {
        client = null;
        serveTime = null;
        timeForOrder = 0;
        clientNumber = -1;

        return;
    }

    public Date endTimeForOrder() {
        return new Date(serveTime.getTime() + (long) timeForOrder);
    }
}
