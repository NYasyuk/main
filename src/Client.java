import java.util.Date;

public class Client {
    private Date arrivalTime;
    private int tWait;

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int gettWait() {
        return tWait;
    }

    public void settWait(int tWait) {
        this.tWait = tWait;
    }

    public Client(Date arrivalTime, int tWait) {
        this.arrivalTime = arrivalTime;
        this.tWait = tWait;
    }

    Date leaveTime() {
        return new Date(arrivalTime.getTime() + (long) tWait);
    }

    Date stopWaiting() {
        return new Date();
    }
}
